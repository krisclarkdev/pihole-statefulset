# Pihole [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/)

Pihole is awesome but every time I've ever come across documentation or tutorials they tend to be simple.  Either deploying using [helm](https://helm.sh/) or  by [manifest](https://prefetch.net/blog/2019/10/16/the-beginners-guide-to-creating-kubernetes-manifests/).  I run my entire house on [Kubernetes](https://kubernetes.io/) including my lab enviornment.  So I wanted to make a highly available [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/) that retained HTTP sessions for the admin portal.  This probably isn't perfect but it meets my needs and I wanted to throw it out on GitLab in case it might help someone else

# Instructions

1. Download the [manifest](https://prefetch.net/blog/2019/10/16/the-beginners-guide-to-creating-kubernetes-manifests/)

```
wget https://gitlab.com/krisclarkdev/pihole-statefulset/-/raw/main/pihole-stateful.yaml
```

2. Base64 your password

```
echo -n 'NOTASECRET' | openssl base64
```

3. Replace "CHANGEME" on line 10

```
  password: CHANGEME
```

4. Remove example local DNS enteries on lines 35 and 36 or add your own

```
    10.0.0.1    some.local.lan1
    10.0.0.2    some.local.lan2 
```

5. Update line 58 to match your storage class, I'm using the [NFS CSI Driver](https://github.com/kubernetes-csi/csi-driver-nfs)

```
storageClassName: nfs-csi
```

6. Update line 132 to match your timezone

```
          value: America/Chicago
```

7. Change the DNS Servers on line 134, 194, and 195 if you want to use something else

```
          value: 1.1.1.1;1.0.0.1
```

```
        - 1.1.1.1
        - 1.0.0.1
```

8. Deploy

```
kubectl create -f ./pihole-stateful.yaml
```

9.  Implement some ingress of your choosing.  I've setup a stream for UDP on port 53 using [Nginx Proxy Manager](https://nginxproxymanager.com/) and a reverse proxy for the web port using the following FQDN's

```
FQDN for DNS Stream
pihole-stateful-dns.pihole-stateful.svc.cluster.local:53

FQDN for Admin portal reverse proxy
pihole-stateful.pihole-stateful.svc.cluster.local:80
```

10. Test

```
dig @ipOfYourIngressOrStream google.com
```

11. Enjoy
